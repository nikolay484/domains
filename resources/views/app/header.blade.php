<html lang="en-gb">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>MyDomainy | @if (!empty($title)) {{$title}} @endif</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/ico">

    <!-- CSS Stylesheets -->
    <link id="default-css" rel="stylesheet" href="{{asset('style.css')}}" type="text/css">
    <link id="shortcodes-css" rel="stylesheet" href="{{asset('css/shortcodes.css')}}" type="text/css">
    <link id="responsive-css" rel="stylesheet" href="{{asset('css/responsive.css')}}" type="text/css">
    <link id="skin-css" rel="stylesheet" href="{{asset('skins/electricblue/style.css')}}" type="text/css">

    <!-- Additional Stylesheets -->
    <link rel="stylesheet" href="{{asset('css/component.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/prettyPhoto.css')}}" type="text/css">
    <link href="{{asset('css/pace-theme-loading-bar.css')}}" rel="stylesheet" media="all" />

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="{{asset('js/layerslider/css/layerslider.css')}}" type="text/css">

    <script type="text/javascript" src="{{asset('js/modernizr-2.6.2.min.js')}}"></script>

    <style>

        #regForm>input {
            width:50%;
            margin: auto;
        }
        #regForm>input[type=submit]{
        }
    </style>

</head>

<body class="page-template-tpl-fullwidth-php">
<!-- **Wrapper** -->
<div class="wrapper">


        <div id="loader-wrapper">
            <div class="loader">
                <!-- the component -->
                @if (!Session::has('guest'))
                <div class="pre-loader"></div>
                @endif
            </div>
        </div>



    <!-- **Header Top Bar** -->
    <div id="bbar-wrapper">
        <div class="container">
            <div class="column dt-sc-one-half first">
                <p class="contact"> Call-us on : <span>+972 547864</span> </p>
            </div>

            <div class="column dt-sc-one-half alignright">
                <div class="buttons">
                    @if (Session::has('user_id'))

                        <a href="{{url('cpanel/profile')}}" title="">CPanel <i class="fa fa-caret-right"></i></a>
                        <a href="{{url('user/logout')}}" title="">Logout <i class="fa fa-caret-right"></i></a>
                        @else
                        <a href="{{url('user/login')}}" title="">Login <i class="fa fa-caret-right"></i></a>
                        <a href="{{url('user/register')}}" title="">Register <i class="fa fa-caret-right"></i></a>

                    @endif

                </div>
            </div>
        </div>
    </div> <!-- **Header Top Bar Ends** -->

    <!-- **Header Wrapper** -->
    <div id="header-wrapper">
        <!-- **Header** -->
        <header id="header" class="header5">
            <div class="container">
                <!-- **Logo** -->
                <div id="logo">
                    <a href="{{url('/')}}"> <h1 style="font-weight: bolder;color: white">My DomainY</h1> </a>
                </div><!-- **Logo Ends** -->

                <!-- **Main-Menu Navigation** -->
                <div id="primary-menu">
                    <div class="dt-menu-toggle" id="dt-menu-toggle">Menu<span class="dt-menu-toggle-icon"></span></div>
                    <nav id="main-menu">
                        <ul id="menu-main-menu" class="menu">
                            <li class="current_page_item menu-item-simple-parent menu-item-depth-0"> <a href="{{url('/')}}" title=""> <span class="menu-icon fa fa-home"> </span> Home </a>
                                <a class="dt-menu-expand">+</a>
                            </li>

                            <li class="menu-item-simple-parent menu-item-depth-0"> <a href="about.html" title=""> <span class="menu-icon fa fa-book"> </span> Pages </a>
                                <a class="dt-menu-expand">+</a>
                            </li>

                            <li class="menu-item-megamenu-parent fullwidth megamenu-5-columns-group menu-item-depth-0"><a href="blog-three-column.html">Blog</a>
                                <a class="dt-menu-expand">+</a>
                            </li>

                            <li class="menu-item-megamenu-parent megamenu-4-columns-group menu-item-depth-0"><a href="portfolio-four-column.html">Portfolio</a>
                                <a class="dt-menu-expand">+</a>
                            </li>

                            <li class="menu-item-megamenu-parent megamenu-3-columns-group menu-item-depth-0 hasImage-bg right-aligned"><a href="#">Websites</a>

                                <a class="dt-menu-expand">+</a>
                            </li>

                            <li class="menu-item-simple-parent menu-item-depth-0"> <a href="contact.html" title=""> <span class="menu-icon fa fa-envelope"> </span> Contact </a>
                            
                            </li>
                        </ul>
                    </nav>
                </div> <!-- **Main-Menu Navigation Ends** -->
            </div>
        </header> <!-- **Header Ends** -->
    </div> <!-- **Header Wrapper Ends** -->