@extends('master')
@section('content')
    <div id="main">
        <!-- **Breadcrumb Section** -->
        <div class="breadcrumb-section">
            <div class="container">
                <h1>Register</h1>
                <div class="breadcrumb">
                    <a href="index.html" title="">Home</a> / <span>404</span>
                </div>
            </div>
        </div> <!-- **Breadcrumb Section Ends** -->

        <!-- **Content Main** -->
        <section class="content-main">

            <!-- **Primary** -->
            <div class="content-full-width" id="primary">

                <div class="fullwidth-section">
                    <div class="fullwidth-bg">
                        <div class="container">

                            <div class="page_info">
                                <h3 class="title center"><span> <i class="fa fa-user"></i></span> Account Center - Member Registration </h3>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="fullwidth-section dt-sc-parallax" style="background-image: url(http://placehold.it/1920x930/&text=Parallax);">
                    <div class="fullwidth-bg">
                        <div class="container">

                            <div class="dt-sc-hr-invisible-medium"></div>
                            <div class="dt-sc-clear"></div>

                            <div class="form-wrapper register">
                                <form method="post" action="{{url('user/register/add')}}" id="regForm" name="frmRegister" autocomplete="off" novalidate="novalidate">
                                    @csrf

                                        <input value="{{old('firstName')}}" type="text" placeholder="First Name" id="firstName" name="firstName" >
                                      <small style="margin: 0 25%;color: #ff2b00;font-weight: bolder">  {{$errors->first('firstName')}}</small>
                                    <input value="{{old('lastName')}}" type="text" placeholder="Last Name" id="lastName" name="lastName">
                                    <small style="margin: 0 25%;color: #ff2b00;font-weight: bolder">  {{$errors->first('lastName')}}</small>
                                    <input value="{{old('email')}}" type="email" placeholder="Email Address" id="email" name="email">
                                    <small style="margin: 0 25%;color: #ff2b00;font-weight: bolder">  {{$errors->first('email')}}</small>
                                    <input value="{{old('webAddress')}}" type="text" placeholder="Web Address" id="webAddress" name="webAddress" autocomplete="off">
                                    <small style="margin: 0 25%;color: #ff2b00;font-weight: bolder">  {{$errors->first('webAddress')}}</small>
                                    <input type="password" placeholder="Password" id="password" name="password">
                                    <small style="margin: 0 25%;color: #ff2b00;font-weight: bolder">  {{$errors->first('password')}}</small>
                                    <input type="password" placeholder="Confirm Password" id="password_confirmation" name="password_confirmation">
                                    <small style="margin: 0 25%;color: #ff2b00;font-weight: bolder">  {{$errors->first('password_confirmation')}}</small>




                                    <input type="submit" class="dt-sc-button small" value="Create an Account" style="float: none;margin: 0 25%">


                                </form>
                            </div>

                            <div class="dt-sc-hr-invisible-medium"></div>
                            <div class="dt-sc-clear"></div>

                        </div>
                    </div>
                </div>

                <div class="fullwidth-section">
                    <div class="fullwidth-bg">
                        <div class="container">

                            <div class="dt-sc-hr-invisible-medium"></div>
                            <div class="dt-sc-clear"></div>

                            <div class="newsletter-container">
                                <h2>Subscribe Newsletter for updates</h2>
                                <div class="dt-sc-one-half column first">
                                    <p>Nam libero tempore, eu volutpat enim diam eget metus cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.</p>
                                </div>

                                <div class="dt-sc-one-half column last">
                                    <form name="frmnewsletter" class="newsletter-form" action="php/subscribe.php" method="post">
                                        <input type="email" required="" placeholder="Enter Your Email ID" name="mc_email">
                                        <input type="submit" value="Submit" class="dt-sc-button" name="btnsubscribe">
                                    </form>
                                    <div id="ajax_subscribe_msg"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div><!-- **Primary - End** -->
        </section> <!-- **Content Main Ends** -->

        <div class="tweet-box">
            <div class="container">
                <div id="tweets_container"></div>
            </div>
        </div>
    </div> <!-- **Main Ends** -->
@endsection