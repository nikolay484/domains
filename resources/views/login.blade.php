@extends('master')
@section('content')
    <div id="main">
        <!-- **Breadcrumb Section** -->
        <div class="breadcrumb-section">
            <div class="container">
                <h1>Log-in</h1>
                <div class="breadcrumb">
                    <a href="index.html" title="">Home</a> / <span>404</span>
                </div>
            </div>
        </div> <!-- **Breadcrumb Section Ends** -->

        <!-- **Content Main** -->
        <section class="content-main">

            <!-- **Primary** -->
            <div class="content-full-width" id="primary">

                <div class="fullwidth-section">
                    <div class="fullwidth-bg">
                        <div class="container">

                            <div class="page_info">
                                <h3 class="title center"><span> <i class="fa fa-user"></i></span> Account Center - Member Login Area </h3>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="fullwidth-section dt-sc-parallax" style="background-image: url(http://placehold.it/1920x930/&text=Parallax);">
                    <div class="fullwidth-bg">
                        <div class="container">

                            <div class="dt-sc-hr-invisible-medium"></div>
                            <div class="dt-sc-clear"></div>

                            <div class="form-wrapper login">
                                <form method="post" action="{{url('user/login/check')}}" id="loginform" name="frmLogin">
                                    @csrf
                                    <p>
                                        <input value="{{old('email')}}" type="text" placeholder="Username or Email" id="email" name="email" autocomplete="off">
                                    </p>

                                    <p>
                                        <input type="password" placeholder="Password" id="password" name="password">
                                    </p>

                                    <label><input type="checkbox" value="forever" id="rememberme" name="rememberme"> Remember Me</label>
                                    <input type="submit" class="dt-sc-button small" value="Log-In">
                                    <small style="color: red;">{{Session::get('loginError')}}</small>
                                </form>
                            </div>

                            <div class="dt-sc-hr-invisible-medium"></div>
                            <div class="dt-sc-clear"></div>

                        </div>
                    </div>
                </div>

                <div class="fullwidth-section">
                    <div class="fullwidth-bg">
                        <div class="container">

                            <div class="dt-sc-hr-invisible"></div>
                            <div class="dt-sc-hr-invisible-small"></div>
                            <div class="dt-sc-clear"></div>

                            <div class="page_info aligncenter">
                                <h4 class="title">Need help logging-in?</h4>
                                <p>If you don't have an account yet, <a href="register.html" title="">Register here</a> and get started..
                                <p>Lost or Forgot your password? You can change it with <a href="#" title="">Reset Password</a> here..
                            </div>

                        </div>
                    </div>
                </div>

            </div><!-- **Primary - End** -->
        </section> <!-- **Content Main Ends** -->

        <div class="tweet-box">
            <div class="container">
                <div id="tweets_container"></div>
            </div>
        </div>
    </div> <!-- **Main Ends** -->
@endsection