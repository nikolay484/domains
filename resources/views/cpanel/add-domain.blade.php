@extends('cpanel.master')
@section('content')
    <!-- content -->
    <div id="content" class="app-content box-shadow-z0" role="main">
        <div class="app-header white box-shadow">
            <div class="navbar navbar-toggleable-sm flex-row align-items-center">
                <!-- Open side - Naviation on mobile -->
                <a data-toggle="modal" data-target="#aside" class="hidden-lg-up mr-3">
                    <i class="material-icons">&#xe5d2;</i>
                </a>
                <!-- / -->

                <!-- Page title - Bind to $state's title -->
                <div class="mb-0 h5 no-wrap" ng-bind="$state.current.data.title" id="pageTitle"></div>

                <!-- navbar collapse -->
                <div class="collapse navbar-collapse" id="collapse">
                    <!-- link and dropdown -->


                    <div ui-include="'../views/blocks/navbar.form.html'"></div>
                    <!-- / -->
                </div>
                <!-- / navbar collapse -->

                <!-- navbar right -->
                <ul class="nav navbar-nav ml-auto flex-row">
                    <li class="nav-item dropdown pos-stc-xs">
                        <a class="nav-link mr-2" href data-toggle="dropdown">
                            <i class="material-icons">&#xe7f5;</i>
                            <span class="label label-sm up warn">3</span>
                        </a>
                        <div ui-include="'../views/blocks/dropdown.notification.html'"></div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link p-0 clear" href="#" data-toggle="dropdown">
                  <span class="avatar w-32">
                    <img src="../assets/images/a0.jpg" alt="...">
                    <i class="on b-white bottom"></i>
                  </span>
                        </a>
                        <div ui-include="'../views/blocks/dropdown.user.html'"></div>
                    </li>
                    <li class="nav-item hidden-md-up">
                        <a class="nav-link pl-2" data-toggle="collapse" data-target="#collapse">
                            <i class="material-icons">&#xe5d4;</i>
                        </a>
                    </li>
                </ul>
                <!-- / navbar right -->
            </div>
        </div>

        <div ui-view class="app-body" id="view">
            <!-- ############ PAGE START-->
            <div class="row-col">

                <div class="col-sm">
                    <div ui-view class="padding pos-rlt">

                        <div>
                            <div class="box">
                                <div class="box-header">
                                    <h2>Add Domain</h2>
                                    <small>Welcome {{Session::get('user_name')}}</small>
                                    <a href="{{url('cpanel/domains')}}" class="md-btn md-raised m-b-sm w-xs red mt-3">back</a>

                                </div>
                                <div class="row no-gutter">

                                    <div class="ml-0 ml-md-4 col-sm-9 col-lg-10 light lt">
                                        <div class="tab-content pos-rlt">
                                            <div class="tab-pane active" id="tab-1" aria-expanded="false">
                                                <div class="pl-4 dker">
                                                    @if (Session::get('user_role') == 'admin' && Session::get('admin_role')=='admin')
                                                        
                                                    <span class="font-weight-bold">Choose User:</span>
                                                    <select name="" id="chooseUser" onselect="getUserId()">
                                                        @foreach($users as $user)
                                                        <option data-id="{{$user->id}}" value="{{$user->firstName . ' ' . $user->lastName}}">{{$user->firstName . ' ' . $user->lastName}}</option>
                                                            @endforeach
                                                    </select>
                                                    @endif

                                                </div>
                                                <div class="p-a-md dker _600">Choose Domain</div>

                                                    {{--<div class="form-group">--}}
                                                    {{--<label>Profile picture</label>--}}
                                                    {{--<div class="form-file">--}}
                                                    {{--<input type="file">--}}
                                                    {{--<button class="btn white">Upload new picture</button>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                <form action="" class="ml-5 p-2 p-md-0" style="background-color: transparent !important;">

                                                    <div class="form-group row">
                                                        <label class="col-1 form-control-label">Http://</label>
                                                        <div class="col-sm-10">
                                                            <div class="row mr-3 mr-md-0">
                                                                <div class="col col-md-4 m-1">
                                                                    <input id="address" type="text" class="form-control" placeholder="adress here">
                                                                </div>
                                                                <div class="col-md-3 m-1">
                                                                    <select id="domain" style="font-size: 15px" class="form-control" >
                                                                        <option value="">Choose domain...</option>
                                                                        <option value=".com">.com</option>
                                                                        <option value=".co.il">.co.il</option>
                                                                        <option value=".org">.org</option>
                                                                        <option value=".net">.net</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <a class="addressDone form-control btn btn-sm btn-info m-1 text-white mt-2">Next</a>
                                                                </div>
                                                                <small class="text-danger">{{$errors->first()}}</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>


                                            </div>
                                            <div class="tab-pane" id="tab-2" aria-expanded="true">
                                                <div class="p-a-md dker _600">Payment</div>
                                                <form method="post" action="{{url('cpanel/domains')}}" role="form" class="p-a-md col-md-6">
                                                    @csrf
                                                    @if (Session::get('user_role') == 'admin' && Session::get('admin_role')=='admin')

                                                    <div class="form-group">
                                                        <label>Chosen User</label>
                                                        <input id="" disabled type="text"  class="choosenUser form-control has-value"
                                                               value="">
                                                        <small class="text-danger error" id=""></small>
                                                        <input id="userName"  name="userId" type="hidden"  class="choosenUserId form-control has-value"
                                                               value="">

                                                    </div>
                                                    @endif
                                                    <div class="form-group">
                                                        <label>Your Adress</label>
                                                        <input id="" disabled type="text"  class="choosedAdress form-control has-value"
                                                               value="">
                                                        <small class="text-danger error" id=""></small>
                                                        <input id="finalAdress"  name="finalAdress" type="hidden"  class="choosedAdress form-control has-value"
                                                               value="">

                                                    </div>
                                                    <div class="form-group">
                                                        <label>Credit Card</label>
                                                        <input id="cc" type="text"  class="form-control has-value"
                                                               value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Date</label>
                                                        <input id="date" type="text" class="form-control has-value"
                                                               value="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>CVV</label>
                                                        <input id="cvv" type="text" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>ID number</label>
                                                        <input id="id" type="text" class="form-control">
                                                    </div>
                                                    <a href="#" class="btn btn-danger m-t next">back</a>
                                                    <input id="orderConfirm" disabled="disabled" type="submit" class=" btn btn-info m-t" value="Next">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- ############ PAGE END-->

        </div>
    </div>
    <!-- / -->
@endsection