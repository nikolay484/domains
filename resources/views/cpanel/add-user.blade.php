@extends('cpanel.master')
@section('content')
    <!-- content -->
    <div id="content" class="app-content box-shadow-z0" role="main">
        <div class="app-header white box-shadow">
            <div class="navbar navbar-toggleable-sm flex-row align-items-center">
                <!-- Open side - Naviation on mobile -->
                <a data-toggle="modal" data-target="#aside" class="hidden-lg-up mr-3">
                    <i class="material-icons">&#xe5d2;</i>
                </a>
                <!-- / -->

                <!-- Page title - Bind to $state's title -->
                <div class="mb-0 h5 no-wrap" ng-bind="$state.current.data.title" id="pageTitle"></div>

                <!-- navbar collapse -->
                <div class="collapse navbar-collapse" id="collapse">
                    <!-- link and dropdown -->


                    <div ui-include="'../views/blocks/navbar.form.html'"></div>
                    <!-- / -->
                </div>
                <!-- / navbar collapse -->

                <!-- navbar right -->
                <ul class="nav navbar-nav ml-auto flex-row">
                    <li class="nav-item dropdown pos-stc-xs">
                        <a class="nav-link mr-2" href data-toggle="dropdown">
                            <i class="material-icons">&#xe7f5;</i>
                            <span class="label label-sm up warn">3</span>
                        </a>
                        <div ui-include="'../views/blocks/dropdown.notification.html'"></div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link p-0 clear" href="#" data-toggle="dropdown">
                  <span class="avatar w-32">
                    <img src="../assets/images/a0.jpg" alt="...">
                    <i class="on b-white bottom"></i>
                  </span>
                        </a>
                        <div ui-include="'../views/blocks/dropdown.user.html'"></div>
                    </li>
                    <li class="nav-item hidden-md-up">
                        <a class="nav-link pl-2" data-toggle="collapse" data-target="#collapse">
                            <i class="material-icons">&#xe5d4;</i>
                        </a>
                    </li>
                </ul>
                <!-- / navbar right -->
            </div>
        </div>

        <div ui-view class="app-body" id="view">
            <!-- ############ PAGE START-->
            <div class="row-col">

                <div class="col-sm">
                    <div ui-view class="padding pos-rlt">

                        <div>
                            <div class="box">
                                <div class="box-header">
                                    <h2>User Profile</h2>
                                    <small>Welcome {{Session::get('user_name')}}</small>
                                </div>
                                <div class="row no-gutter">
                                    <div class="col-sm-3 col-lg-2">
                                        <div class="p-y">
                                            <div class="nav-active-border left b-primary">
                                                <ul class="nav nav-sm flex-column">
                                                    <li class="nav-item">
                                                        <a class="nav-link block active" href="" data-toggle="tab" data-target="#tab-1" aria-expanded="false">Profile</a>
                                                    </li>


                                                    <li class="nav-item">
                                                        <a class="nav-link block" href="" data-toggle="tab" data-target="#tab-5" aria-expanded="false">Security</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 col-lg-10 light lt">
                                        <div class="tab-content pos-rlt">
                                            <div class="tab-pane active" id="tab-1" aria-expanded="false">
                                                <div class="p-a-md dker _600">Public profile</div>
                                                <form role="form" class="p-a-md col-md-6" method="post" action="{{url('cpanel/users/')}}">
                                                    @csrf

                                                    {{--<div class="form-group">--}}
                                                        {{--<label>Profile picture</label>--}}
                                                        {{--<div class="form-file">--}}
                                                            {{--<input type="file">--}}
                                                            {{--<button class="btn white">Upload new picture</button>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                    <div class="form-group">
                                                        <label>First Name</label>
                                                        <input name="firstName" type="text" class="form-control" value="{{old('firstName')}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last Name</label>
                                                        <input name="lastName" type="text" class="form-control" value="{{old('lastName')}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>URL</label>
                                                        <input name="webAddress" type="text" class="form-control" value="{{old('webAddress')}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input name="email" type="text" class="form-control" value="{{old('email')}}">
                                                    </div>

                                                    <input  type="button" class="btn btn-info m-t switch-tab" value="Next">
                                                    <small style="color:red;">{{$errors->first()}}</small>

                                            </div>
                                            <div class="tab-pane" id="tab-5" aria-expanded="false">
                                                <div class="p-a-md dker _600">Security</div>
                                                <div class="p-a-md">
                                                    <div class="clearfix m-b-lg">



                                                            <div class="form-group">
                                                                <label>Password</label>
                                                                <input name="password" type="password" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Confirm Password</label>
                                                                <input name="password_confirmation" type="password" class="form-control">
                                                            </div>
                                                        <input  type="button" class="btn btn-warning m-t switch-tab" value="Back">

                                                        <input type="submit" class="btn btn-info m-t" value="Create">
                                                            <br>
                                                            <small style="color: red;">{{$errors->first()}}</small>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- ############ PAGE END-->

        </div>
    </div>
    <!-- / -->
@endsection