<!-- ############ LAYOUT END-->

</div>
<!-- build:js scripts/app.html.js -->
<!-- jQuery -->
<script src="{{asset('cpanel/libs/jquery/jquery/dist/jquery.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('cpanel/libs/jquery/tether/dist/js/tether.min.js')}}"></script>
<script src="{{asset('cpanel/libs/jquery/bootstrap/dist/js/bootstrap.js')}}"></script>
<!-- core -->
<script src="{{asset('cpanel/libs/jquery/underscore/underscore-min.js')}}"></script>
<script src="{{asset('cpanel/libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js')}}"></script>
<script src="{{asset('cpanel/libs/jquery/PACE/pace.min.js')}}"></script>

<script src="{{asset('scripts/config.lazyload.js')}}"></script>

<script src="{{asset('cpanel/scripts/palette.js')}}"></script>
<script src="{{asset('cpanel/scripts/ui-load.js')}}"></script>
<script src="{{asset('cpanel/scripts/ui-jp.js')}}"></script>
<script src="{{asset('cpanel/scripts/ui-include.js')}}"></script>
<script src="{{asset('cpanel/scripts/ui-device.js')}}"></script>
<script src="{{asset('cpanel/scripts/ui-form.js')}}"></script>
<script src="{{asset('cpanel/scripts/ui-nav.js')}}"></script>
<script src="{{asset('cpanel/scripts/ui-screenfull.js')}}"></script>
<script src="{{asset('cpanel/scripts/ui-scroll-to.js')}}"></script>
<script src="{{asset('cpanel/scripts/ui-toggle-class.js')}}"></script>

<script src="{{asset('cpanel/scripts/app.js')}}"></script>

<!-- ajax -->
<script src="{{asset('cpanel/libs/jquery/jquery-pjax/jquery.pjax.js')}}"></script>
<script src="{{asset('cpanel/scripts/ajax.js')}}"></script>
<!-- endbuild -->


{{--custom js--}}
<script src="{{asset('cpanel/scripts/custom.js')}}"></script>
{{--javascript toastr--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

@if(Session::get('sm'))
    <script>
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "slideDown",
            "hideMethod": "slideUp",

        };
        toastr["{{Session::get('type')}}"]("{{session::get('sm')}}", "{{session::get('sb')}}");


    </script>
@endif


<script>
    //change color of cpanel on admin
    @if (Session::get('user_role') == 'admin' && Session::get('admin_role') == 'admin')
        $('.left.navside').removeClass('dark').addClass('info');
        $('.app-header').removeClass('white').addClass('info');
        $('.navbar').removeClass('white').addClass('info');
        $('.app-content').removeClass('white');
    @endif
</script>
</body>
</html>
