@extends('cpanel.master')
@section('content')
    <!-- content -->
    <div id="content" class="app-content box-shadow-z0" role="main">
        <div class="app-header white box-shadow">
            <div class="navbar navbar-toggleable-sm flex-row align-items-center">
                <!-- Open side - Naviation on mobile -->
                <a data-toggle="modal" data-target="#aside" class="hidden-lg-up mr-3">
                    <i class="material-icons">&#xe5d2;</i>
                </a>
                <!-- / -->

                <!-- Page title - Bind to $state's title -->
                <div class="mb-0 h5 no-wrap" ng-bind="$state.current.data.title" id="pageTitle"></div>

                <!-- navbar collapse -->
                <div class="collapse navbar-collapse" id="collapse">
                    <!-- link and dropdown -->


                    <div ui-include="'../views/blocks/navbar.form.html'"></div>
                    <!-- / -->
                </div>
                <!-- / navbar collapse -->

                <!-- navbar right -->
                <ul class="nav navbar-nav ml-auto flex-row">
                    <li class="nav-item dropdown pos-stc-xs">
                        <a class="nav-link mr-2" href data-toggle="dropdown">
                            <i class="material-icons">&#xe7f5;</i>
                            <span class="label label-sm up warn">3</span>
                        </a>
                        <div ui-include="'../views/blocks/dropdown.notification.html'"></div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link p-0 clear" href="#" data-toggle="dropdown">
                  <span class="avatar w-32">
                    <img src="../assets/images/a0.jpg" alt="...">
                    <i class="on b-white bottom"></i>
                  </span>
                        </a>
                        <div ui-include="'../views/blocks/dropdown.user.html'"></div>
                    </li>
                    <li class="nav-item hidden-md-up">
                        <a class="nav-link pl-2" data-toggle="collapse" data-target="#collapse">
                            <i class="material-icons">&#xe5d4;</i>
                        </a>
                    </li>
                </ul>
                <!-- / navbar right -->
            </div>
        </div>

        <div ui-view class="app-body" id="view">
            <!-- ############ PAGE START-->
            <div class="row-col">

                <div class="col-sm">
                    <div ui-view class="padding pos-rlt">

                        <div>
                            <div class="box">
                                <div class="box-header">
                                    <h2>User Profile</h2>
                                    <small>Welcome {{Session::get('user_name')}}</small>
                                    <a href="{{url('cpanel/domains/create')}}" class="md-btn md-raised m-b-sm w-xs blue" style="margin-top: 5px">add new</a>
                                </div>
                                <div class="table-responsive">

                                    @if (!empty($domains))
                                        <table class="table table-bordered m-0">
                                        <thead>
                                        <tr>

                                            <th>Domain</th>
                                            <th>Status</th>
                                            @if (Session::get('admin_role')=='admin')
                                                <th>User</th>
                                            @endif
                                            <th>Operation</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                     @foreach ($domains as $domain)
                                         <tr>

                                             <td>{{$domain->domain}}</td>
                                             <td>
                                                 @if ($domain->status == true)
                                                     <p class="text-success">active</p>
                                                     @else
                                                     <p class="text-danger">closed</p>
                                                 @endif

                                             </td>
                                             @if (Session::get('admin_role')=='admin')
                                                 <td>
                                                     {{$domain->firstName . ' ' . $domain->lastName}}
                                                 </td>
                                             @endif


                                             <td>
                                                 <form action="{{url('cpanel/domains/' . $domain->id)}}" class="deleteForm" method="post">
                                                     @csrf
                                                     @method('delete')
                                                 <input type="submit" data-id="{{$domain->id}}" class="btn btn-sm warn submit"  value="Delete">
                                                 </form>
                                                 <form action="{{url('cpanel/domains/status')}}" method="post" class="mt-1">
                                                     @csrf
                                                     <input type="hidden" name="id" value="{{$domain->id}}">
                                                 @if ($domain->status == true)
                                                     <input type="submit" name="status" class="btn btn-sm danger" value="Deactivate">
                                                 @else
                                                     <input type="submit" name="status"  class="btn btn-sm success" value="Activate">
                                                 @endif
                                                 </form>
                                             </td>


                                         </tr>
                                     @endforeach
                                        </tbody>
                                    </table>
                                        @else
                                        <div>
                                            <p class="ml-5"><i>Your Domain List Is Empty</i></p>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- ############ PAGE END-->

        </div>
    </div>
    <!-- / -->
@endsection