<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Domainy | @if (!empty($title))
            {{$title}}
        @endif</title>
    <meta name="description" content="Admin, Dashboard, Bootstrap, Bootstrap 4, Angular, AngularJS"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="../assets/images/logo.png">
    <meta name="apple-mobile-web-app-title" content="Flatkit">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="../assets/images/logo.png">

    {{--javascript toast--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"
          type="text/css">
    <!-- style -->
    <link rel="stylesheet" href="{{asset('cpanel/assets/animate.css/animate.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('cpanel/assets/glyphicons/glyphicons.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('c[anel/assets/font-awesome/css/font-awesome.min.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{asset('cpanel/assets/material-design-icons/material-design-icons.css')}}"
          type="text/css"/>

    <link rel="stylesheet" href="{{asset('cpanel/assets/bootstrap/dist/css/bootstrap.min.css')}}" type="text/css"/>
    <!-- build:css ../assets/styles/app.min.css -->
    <link rel="stylesheet" href="{{asset('cpanel/assets/styles/app.css')}}" type="text/css"/>
    <!-- endbuild -->
    <link rel="stylesheet" href="{{asset('cpanel/assets/styles/font.css')}}" type="text/css"/>
    <script>
        const MAIN_PATH = "{{url('/')}}/";
    </script>
</head>
<body>
<div class="app" id="app">

    <!-- ############ LAYOUT START-->

    <!-- aside -->
    <div id="aside" class="app-aside modal nav-dropdown">
        <!-- fluid app aside -->
        <div class="left navside dark dk" data-layout="column">
            <div class="navbar no-radius">
                <!-- brand -->
                <a class="navbar-brand">
                    <div ui-include="'../assets/images/logo.svg'"></div>
                    <img src="../assets/images/logo.png" alt="." class="hide">
                    <span class="hidden-folded inline">Domainy</span>
                </a>
                <!-- / brand -->
            </div>
            <div class="hide-scroll" data-flex>
                <nav class="scroll nav-light">

                    <ul class="nav" ui-nav>
                        <li class="nav-header hidden-folded">
                            <small class="text-muted">Main</small>
                        </li>

                        <li>
                            <a href="{{url('cpanel/profile')}}">
                    <span class="nav-icon">
                      <i class="material-icons">&#xe3fc;
                        <span ui-include="'../assets/images/i_0.svg'"></span>
                      </i>
                    </span>
                                <span class="nav-text">profile</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('cpanel/domains')}}">
                    <span class="nav-icon">
                      <i class="material-icons">&#xe3fc;
                        <span ui-include="'../assets/images/i_0.svg'"></span>
                      </i>
                    </span>
                                <span class="nav-text">domains</span>
                            </a>
                        </li>
                        @if (Session::get('admin_role')=='admin')
                            <li>
                                <a href="{{url('cpanel/users')}}">
                    <span class="nav-icon">
                      <i class="material-icons">&#xe3fc;
                        <span ui-include="'../assets/images/i_0.svg'"></span>
                      </i>
                    </span>
                                    <span class="nav-text">users</span>
                                </a>
                            </li>
                        @endif

                    </ul>
                </nav>
            </div>
            <div class="b-t">
                <div class="nav-fold">

                    @if (Session::get('user_role')=='admin')

                        @if (Session::get('admin_role')=='user')
                            <a href="{{url('cpanel/switch-role')}}">Switch to Admin</a>
                        @else
                            <a href="{{url('cpanel/switch-role')}}">Switch to User</a>

                        @endif
                        <br>
                    @endif
                    <a href="{{url('user/logout')}}" class="">logout</a> -
                    <a href="{{url('/')}}">back to web</a>

                </div>
            </div>
        </div>
    </div>
    <!-- / -->