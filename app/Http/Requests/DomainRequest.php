<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DomainRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [

            'finalAdress' => "required|url||unique:domains,domain",


        ];
    }

    public function messages()
    {

        return [
            'finalAdress.required' => 'The Adress is required',
            'finalAdress.unique' => 'Sorry this domain is taken',
            'finalAdress.url' => 'Must be valid URL',

        ];
    }

}
