<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required_unless:cpanel,password|min:2|max:255',
            'lastName' => 'required_unless:cpanel,password|min:2|max:255',
            'email' => 'required_unless:cpanel,password|email',
            'webAddress' => 'url',
            'password' => 'required_unless:cpanel,edit|confirmed',
        ];
    }
    public function messages()
    {
        return [
            'firstName.required_unless' => 'the First Name is required',
            'lastName.required_unless' => 'the Last Name is required',
            'email.required_unless' => 'the email is required',
            'password.required_unless' => 'the password is required',

        ];
    }
}
