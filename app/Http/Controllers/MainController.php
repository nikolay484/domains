<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MainController extends Controller
{
    public function homePage(){

        self::$data['title'] = 'Home Page';
        Session::put('guest' , true);
        return view('main',self::$data);
    }
}
