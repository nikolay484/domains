<?php

namespace App\Http\Controllers\cpanel;

use App\Domain;
use App\Http\Requests\DomainRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;


class DomainsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        self::$data['title'] = 'domain manager';
        self::$data['user'] = User::find(Session::get('user_id'));

        self::$data['domains'] = DB::table('domains')
            ->where('userId', 'like', Session::get('user_id'))
            ->get()
            ->toArray();

        if (Session::get('admin_role') == 'admin') {
            self::$data['domains'] = DB::table('domains AS d')
                ->join('users AS u', 'u.id', '=', 'd.userId')
                ->select('d.*', 'u.firstName', 'u.lastName')
                ->get()
                ->toArray();
        }

        return view('cpanel.domains', self::$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        self::$data['title'] = 'add domain';
        self::$data['users'] = User::createDomain();
        return view('cpanel.add-domain', self::$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(DomainRequest $request)
    {

        Domain::addDomain($request);
        Session::flash('sm', 'Domain added sucessfully');
        Session::flash('type', 'success');
        return redirect('cpanel/domains');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Domain::find($id)->delete();
        Session::flash('sm', 'Domain deleted sucessfully');
        Session::flash('type', 'success');
        return redirect('cpanel/domains');
    }

    public function statusChange(Request $request)
    {

        Domain::changeStatus($request);
        Session::flash('sm', 'Status changed successfully');
        Session::flash('type', 'success');
        return redirect('cpanel/domains');
    }
}
