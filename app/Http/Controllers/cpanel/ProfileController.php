<?php

namespace App\Http\Controllers\cpanel;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{

    public function index()
    {
        self::$data['title'] = 'Profile';
        self::$data['user'] = User::find(Session::get('user_id'));
        return view('cpanel.main', self::$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        self::$data['title']='edit profiel';
        self::$data['user'] = User::find($id);
        return view('cpanel.edit-profile', self::$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        User::updateUser($request, $id);
        Session::flash('sm', 'User updated sucessfully');
        Session::flash('type', 'success');
        return redirect('cpanel/profile');
    }
    public function destroy($id)
    {

    }

    public function switchRole(){
        if (Session::get('admin_role')=='user'){
            Session::put('admin_role', 'admin');
        } else {
            Session::put('admin_role', 'user');

        }
        return redirect('cpanel/profile');
    }
}
