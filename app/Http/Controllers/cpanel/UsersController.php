<?php

namespace App\Http\Controllers\cpanel;

use App\Domain;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('AdminGuard');
    }

    public function index()
    {
        self::$data['title'] = 'user';
        self::$data['users'] = DB::table('users AS u')
            ->join('user_roles AS ur', 'u.id', 'like', 'ur.userId')
            ->select('u.*', 'ur.userRole')
            ->get();

        return view('cpanel/users', self::$data);
    }

    public function create()
    {
        self::$data['title'] = 'add user';
        return view('cpanel.add-user', self::$data);
    }

    public function store(UserRequest $request)
    {
        User::addUser($request);
        Session::flash('sm', 'User added sucessfully');
        Session::flash('type', 'success');
        return redirect('cpanel/users');
    }

    public function show($id)
    {
        self::$data['title'] = 'user';
        self::$data['user'] = User::find($id);
        return view('cpanel.view-user', self::$data);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {

        User::deleteUser($id);
        Domain::deleteUserDomains($id);
        Session::flash('sm', 'User deleted sucessfully');
        Session::flash('type', 'success');
        return redirect('cpanel/users');
    }

    public function ban($uid)
    {
        User::banStatus($uid);
        return redirect('cpanel/users');
    }
}
