<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
//    register page
    public function registerPage()
    {
        self::$data['title'] = 'Register';
        return view('register', self::$data);
    }

//    the register post action
    public function registerNew(UserRequest $request)
    {
        User::registerNew($request);
        return redirect('user/login');
    }

//    the ogin page
    public function loginPage()
    {
        self::$data['title'] = 'Login';
        return view('login', self::$data);
    }

//    the login check via DB
    public function loginVerify(Request $request)
    {
        User::loginCheck($request, $verifyed);
        Session::put('loginError', '');
        if (!$verifyed) {
            Session::flash('loginError', 'Sorry Invalid Email Or Password');
            return redirect()->back();
        } else {
            Session::forget('loginError');
            return redirect('/');
        }
    }
//    logout
    public function logout(){
        Session::flush();
        return redirect('/');


    }
//    switch to user (admin)
public function switchToUser(){
        Session::put('user_role' , 'adminIsUser');
}
}
