<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Http\Middleware\BanGuard;

class Controller extends BaseController
{
    public static $data = [];

    public function __construct()
    {
        $this->middleware('BanGuard');
        self::$data['title'] = '';
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
