<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class loggedControl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('user_id')){
            return redirect('/');
        } else
        return $next($request);
    }
}
