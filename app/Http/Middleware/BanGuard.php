<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class BanGuard
{

    public function handle($request, Closure $next)
    {
        if (Session::get('user_status') == 'banned') {
            return redirect('404');
        } else {
            return $next($request);

        }

    }
}
