<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Domain extends Model
{
    static public function addDomain($request)
    {
        $domain = new self();
        $domain->domain = $request['finalAdress'];
        if ($request['userId'] == true) {
            $domain->userId = $request['userId'];
        } else {
            $domain->userId = Session::get('user_id');

        }
        $domain->save();


    }

    static public function changeStatus($request)
    {


        $domain = self::find($request['id']);

        if ($request['status'] == 'Deactivate') {

            $domain->status = 0;
        }
        if ($request['status'] == 'Activate') {

            $domain->status = 1;
        }

        $domain->save();

    }
    static public function deleteUserDomains($id){
        self::where('userId','=',$id)->delete();
    }
}
