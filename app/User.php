<?php

namespace App;

use Dotenv\Validator;
use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class User extends Model
{
    //new register
    public static function registerNew($request)
    {
        $password = bcrypt($request['password']);
        $user = new self();
        $user->firstName = $request['firstName'];
        $user->lastName = $request['lastName'];
        $user->webAddress = $request['webAddress'];
        $user->email = $request['email'];
        $user->password = $password;
        $user->save();

        //define the user role
        $user = self::all()->last();
        $userRole = new UserRole();
        $userRole->userId = $user->id;
        $userRole->save();


    }

//login
    public static function loginCheck($request, &$verifyed)
    {
        $password = $request['password'];
        $user = self::all();
        if ($user = $user->firstWhere('email', 'like', $request['email'])) {
            if (Hash::check($password, $user->password)) {
                $userRole = UserRole::all()->firstWhere('userId', 'like', $user->id);
                Session::put('user_id', $user->id);
                Session::put('user_status', $user->status);
                Session::put('user_name', $user->firstName . ' ' . $user->lastName);
                if ($userRole->userRole == 7) {
                    Session::put('user_role', 'admin');
                    Session::put('admin_role', 'admin');
                }
                $verifyed = true;
            }
        } else {
            $verifyed = false;
        }


    }

    //update user
    public static function updateUser($request, $id)
    {
        if ($request['cpanel'] == 'edit') {
            $user = User::find($id);
            $user->firstName = $request['firstName'];
            $user->lastName = $request['lastName'];
            $user->webAddress = $request['webAddress'];
            $user->email = $request['email'];
            $user->save();
        }

        if ($request['cpanel'] == 'password') {
            $user = User::find($id);
            $oldPassword = $user->password;
            if (Hash::check($request['old_password'], $user->password)) {
                $newPassword = bcrypt($request['password']);
                $user->password = $newPassword;
                $user->save();
            }
        }
    }

//    user ban status
    static public function banStatus($uid)
    {
        $user = User::find($uid);
        if ($user->status == 'active') {
            $user->status = 'banned';
            Session::flash('sm', 'User baned sucessfully');
            Session::flash('type', 'success');
        } else {
            $user->status = 'active';
            Session::flash('sm', 'User activated sucessfully');
            Session::flash('type', 'success');
        }
        $user->save();
    }

    static public function createDomain()
    {
        $userCount = User::count();
        $userSkip = 0;
        $userLength = $userCount - $userSkip;
        return self::take($userLength)->skip($userSkip)->get();
    }
    static public function deleteUser($id){
        self::find($id)->delete();
    }

    //create user
    static public function addUser($request){
        $password = bcrypt($request['password']);
        $user = new self();
        $user->firstName = $request['firstName'];
        $user->lastName = $request['lastName'];
        $user->webAddress = $request['webAddress'];
        $user->email = $request['email'];
        $user->password = $password;
        $user->save();

        //define the user role
        $user = self::all()->last();
        $userRole = new UserRole();
        $userRole->userId = $user->id;
        $userRole->save();
    }
}
