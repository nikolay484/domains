<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@homePage');

Route::group(['prefix' => 'user'], function () {
    Route::get('register', 'UserController@registerPage')->middleware(['loggedControl']);
    Route::post('register/add', 'UserController@registerNew')->middleware(['loggedControl']);
    Route::get('login', 'UserController@loginPage')->middleware(['loggedControl']);
    Route::post('login/check', 'UserController@loginVerify')->middleware(['loggedControl']);
    Route::get('logout', 'UserController@logout');
    Route::get('ban/{uid}/status', 'cpanel\UsersController@ban');
});

Route::group(['prefix' => 'cpanel'], function () {


Route::group(['middleware' => ['CpanelGuard']], function () {
    Route::resource('profile', 'cpanel\ProfileController');
    Route::resource('domains', 'cpanel\DomainsController');
    Route::resource('users', 'cpanel\UsersController')->middleware(['AdminGuard']);
    Route::post('domains/status', 'cpanel\DomainsController@statusChange');
    Route::get('switch-role','cpanel\ProfileController@switchRole');
});
});